---
title: "Why DuckDuckGo will never completely replace Google Search"
date: 2019-02-04T21:46:54+05:30
author: JB
draft: true
featuredImg: "https://cdn-images-1.medium.com/max/1600/1*BZyse4LuheolKSv8_rlMVA.png"
tags: 
  - Google
  - Privacy
  - Duckduckgo
  - Digital Rights
  - JB speaks
---
---
I recently saw a video highlighting the difference between DuckDuckGo and Google. The video was kind of biased towards DuckDuckGo since it does not collect information on the users like Google Search does. While I think it is good in terms of privacy, I am going to focus on how helpful these search engines are.

Here’s a brief on what I know on how a search engine works. When you give a set of keywords to the search engine it finds web pages on the internet that contains those keywords. Higher the number of times those keywords appear on a webpage, the higher is the rank of the webpage. The search engine shows the page with the highest rank first. There are other factors that search engines take into account, but I believe this is the most important one.

Google is a company that aims at organizing the information available on the internet and making it accessible to everyone. Google understands that search results that are most relevant to one user may not be the most relevant for another user. So Google collects information about the users and predicts which webpage contains information which is most relevant to that particular user.

DuckDuckGo, on the other hand, doesn’t store user info and hence the search results that appear on DuckDuckGo are not curated to your interests. It shows unbiased search results. This is because DuckDuckGo aims at showing unbiased search results and being a search engine that doesn’t store information on the users.

Both methods of displaying search results have their pros and cons.

If I search for “Events near me” on Google, it will take my interests into account to suggest events that match my interests. Which is really convenient. If I search for the same thing on DuckDuckGo, it shows web pages that contain the most number of occurrences of the words ‘events’, ‘near’, and ‘me’. Which isn’t really convenient.

If I search for “Is global warming a hoax”, I am actually looking for an unbiased answer. Most users who search for such debatable topics are also looking for unbiased answers with an open mind. In that case, Google’s biased search results are not more relevant to me than the unbiased search results I’d get from DuckDuckGo.

DuckDuckGo will never replace Google Search. Google Search will never replace DuckDuckGo as well. They serve the same purpose, which is enabling people to navigate the internet easily. But their methods are different. Hence we should treat them as two different tools that are used for two different purposes. Google Search for personalized search results. DuckDuckGo for unbiased search results.

However, I do feel that at some levels Google’s methods are a violation of our digital rights. But I am not sure what our digital rights exactly are since they aren’t defined, and how we’ll be able to make technological advances while protecting those rights.
---